import sbt._

object Dependencies {

  /*
   * Versions
   */
  val betterFilesVersion = "3.9.2"
  val scalatestVersion = "3.2.15"

  /*
   * Libraries: scala
   */
  val betterFiles = "com.github.pathikrit" %% "better-files" % betterFilesVersion
  val scalatest = "org.scalatest" %% "scalatest" % scalatestVersion

}
