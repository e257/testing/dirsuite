
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "2.0.7")

addSbtPlugin("org.wartremover" % "sbt-wartremover" % "3.0.9")

addSbtPlugin("org.xerial.sbt" % "sbt-sonatype" % "3.9.17")

